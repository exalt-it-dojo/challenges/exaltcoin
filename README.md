# Créer votre monnaie/système d'échange local (SEL)!

## Descriptions

**Voici les spécifications minimales:**

* Un porte-monnaie par personne.
* Envoi de SEL entre porte-monnaie.
* Validation ou refus de la réception d'une transaction par le destinataire.
* Crédit après validations respectives.
* Suivre l'état de la validation par l'émetteur
* UI utilisateur
 
**Critères d'évaluations:**

* Gestion des erreurs de transmission ✅
* Alertes (erreurs, refus, ... ) ✅
* Historique des transactions ✅
* Suivi de la plateforme ✅
* Bonus : chiffrement ✅
* Bonus : Temps réel ✅
* SD² ✅



__Langage libre - Implémentation libre - Architecture libre - Plateforme libre__

__Livrable : dépôt git avec doc de mise en œuvre (Groupe Gitlab Exalt-IT préféré)__

_Seul, en squad, tribe, chapter ou guild ..._


Design Pattern mises en jeux : Client-Server 2way messaging, 3way handshake, distributed architechture, messages broadcasting
Une architecture blockchain n'est pas requise, le challenge est plutôt orienté clients/serveur.

## Sequence diagram

```mermaid
sequenceDiagram
    actor A as Alpha
    actor B as Bravo
    participant S as Sierra
    A-)S: Hello I'm here
    B-)S: Hello I'm here
    A-)S: Hello Sierra, where's Bravo?
    S-)A: Here's Bravo's location!
    A->>B: I have some coins for you!
    activate B
    B->>A: Ok, I'm agree
    A->>B: Sending X coins from my wallet.
    B->>A: Ok coins stored in my wallet
    deactivate B
```

## Debug configuration

Pour lancer le projet en mode Debug, reproduire les étapes suivantes :

* Télécharger Tibco Rendezvous
* Cloner le projet ExaltIT.Kata.SystemEchangeLocal
* Builder la solution
* Copier les dll suivantes dans les dossiers bin de Alpha / Beta et Sierra :
*	- C:\tibco\tibrv\8.5\bin\tibrv.dll
*	- C:\tibco\tibrv\8.5\bin\tibrvcm.dll
* Copier le fichier config\Identity(Alpha).xml dans le dossier bin de Alpha et le renommer en Identity.xml
* Copier le fichier config\Identity(Bravo).xml dans le dossier bin de Bravo et le renommer en Identity.xml
* Lancer le projet Sierra, il lance les services TibcoRendezVous.
* Lancer les clients.
